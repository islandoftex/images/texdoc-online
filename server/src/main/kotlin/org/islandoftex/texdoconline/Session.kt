// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline

import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader

/**
 * Data for the whole server session.
 */
object Session {
    /**
     * The version of the API we currently serve. This follows semantic versioning
     * and does not necessarily correspond to the version of texdoc-online.
     */
    const val API_VERSION = "1.0.0"

    /**
     * Cache for CTANTopics.
     */
    val cache = Cache()

    /**
     * The path to the TeX installation determined by getting TEXMFDIST using kpsewhich
     * and hoping that the parent directory actually is the root for a TeX Live installation
     * or at least that the tlpdb is in a sibling directory.
     */
    val texmfDist by lazy {
        val process = ProcessBuilder("kpsewhich", "--var-value=TEXMFDIST").start()
        val input = BufferedReader(InputStreamReader(process.inputStream))
        input.readLines().first()
    }

    /**
     * A resource directory to overwrite the bundled HTML resource.
     */
    var resourceDirectory: File? = null
}
