// SPDX-License-Identifier: BSD-3-Clause

import java.security.MessageDigest
import java.util.Base64
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar
import org.gradle.api.internal.project.ProjectInternal
import org.gradle.api.java.archives.internal.DefaultManifest
import org.gradle.api.tasks.testing.logging.TestLogEvent
import org.islandoftex.texdoconline.build.Versions
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

repositories {
  mavenCentral()
  maven {
    // TeXdoc API
    url = uri("https://gitlab.com/api/v4/projects/14399565/packages/maven")
  }
  maven {
    // CTAN API
    url = uri("https://gitlab.com/api/v4/projects/20625607/packages/maven")
  }
}

plugins {
  val versions = org.islandoftex.texdoconline.build.Versions
  kotlin("jvm")
  application
  id("com.github.johnrengelman.shadow") version versions.shadow
  id("org.jetbrains.dokka") version versions.dokka
}

dependencies {
  implementation(project(":shared"))
  implementation(kotlin("stdlib", Versions.kotlin))
  implementation("com.github.ajalt.clikt:clikt:${Versions.clikt}")
  implementation("io.ktor:ktor-server-netty:${Versions.ktor}")
  implementation("io.ktor:ktor-jackson:${Versions.ktor}")
  implementation("io.ktor:ktor-html-builder:${Versions.ktor}")
  implementation("org.jetbrains.kotlinx:kotlinx-datetime:${Versions.datetime}")
  implementation("org.islandoftex:texdoc:${Versions.texdocapi}")
  implementation("org.islandoftex:ctanapi:${Versions.ctanapi}")
  runtimeOnly("org.slf4j:slf4j-simple:${Versions.slf4j}")
  testImplementation("io.kotest:kotest-runner-junit5-jvm:${Versions.kotest}")
  testImplementation("io.kotest:kotest-assertions-core-jvm:${Versions.kotest}")
  testRuntimeOnly("org.slf4j:slf4j-simple:${Versions.slf4j}")
}

val moduleName = "$group"

java {
  sourceCompatibility = JavaVersion.VERSION_1_8
  targetCompatibility = JavaVersion.VERSION_1_8
}

application {
  applicationName = rootProject.name
  mainClass.set("$moduleName.CLIKt")
}

val appManifest: Manifest by extra(DefaultManifest((project as ProjectInternal)
    .fileResolver).apply {
  attributes["Implementation-Title"] = project.name
  attributes["Implementation-Version"] = project.version
  attributes["Main-Class"] = "$moduleName.CLIKt"
  if (java.sourceCompatibility < JavaVersion.VERSION_1_9) {
    attributes["Automatic-Module-Name"] = moduleName
  }
})

tasks {
  create<Copy>("copyJSFrontend") {
    from(project(":client").tasks.named("browserDistribution"))
    into("build/resources/main/org/islandoftex/texdoconline/js")

    rename { it.replaceBefore(".", "texdoc-online") }
  }
  create<Copy>("htmlFilesWithChecksum") {
    dependsOn("copyJSFrontend")
    outputs.upToDateWhen { false }

    from(
      "src/main/resources/org/islandoftex/texdoconline/index.html",
      "src/main/resources/org/islandoftex/texdoconline/missing.html"
    )
    into("build/resources/main/org/islandoftex/texdoconline/")

    filter { content ->
      val clientHash = MessageDigest.getInstance("SHA-512").run {
        file("build/resources/main/org/islandoftex/texdoconline/js/texdoc-online.js").inputStream().use { stream ->
          val buffer = ByteArray(8192)
          var read = 0
          while (stream.read(buffer).also { read = it } > 0) {
            update(buffer, 0, read)
          }
        }
        digest()
      }.let { Base64.getEncoder().encodeToString(it) }
      content.replace("\$client_js_resource_hash", "sha512-$clientHash")
    }
  }
  named<ProcessResources>("processResources") {
    outputs.upToDateWhen { false }
    dependsOn("copyJSFrontend")
    finalizedBy("htmlFilesWithChecksum")
  }

  withType<KotlinCompile> {
    dependsOn("htmlFilesWithChecksum")
    kotlinOptions.jvmTarget = "1.8"
  }
  named<Jar>("jar") {
    manifest.attributes.putAll(appManifest.attributes)
    archiveAppendix.set("jdk" + java.targetCompatibility.majorVersion)
  }
  named<ShadowJar>("shadowJar") {
    manifest.attributes.putAll(appManifest.attributes)
    archiveAppendix.set("jdk" + java.targetCompatibility.majorVersion + "-with-deps")
    archiveClassifier.set("")
  }
  named<Task>("assembleDist").configure { dependsOn("shadowJar") }
  withType<Test>().configureEach {
    useJUnitPlatform()
    testLogging {
      events(
        TestLogEvent.FAILED, TestLogEvent.PASSED, TestLogEvent.SKIPPED,
        TestLogEvent.STANDARD_ERROR, TestLogEvent.STANDARD_OUT
      )
    }
  }
}
