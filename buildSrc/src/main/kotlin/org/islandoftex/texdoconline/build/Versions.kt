// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline.build

object Versions {
    // plugin dependencies
    const val detekt = "1.19.0"
    const val dokka = "1.8.20"
    const val kotlin = "1.9.0"
    const val shadow = "8.1.1"
    const val spotless = "6.20.0"
    const val spotlessChangelog = "3.0.2"
    const val versionsPlugin = "0.47.0"

    // non-plugin dependencies
    const val clikt = "4.1.0"
    const val coroutines = "1.7.2"
    const val ctanapi = "0.1.0"
    const val datetime = "0.4.0"
    const val html = "0.9.1"
    const val kotest = "5.6.2"
    const val ktor = "1.6.8"
    const val serialization = "1.5.1"
    const val slf4j = "2.0.7"
    const val texdocapi = "1.0.0"
}
