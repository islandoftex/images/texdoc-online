// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline.frontend

import kotlinx.browser.document
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.html.*
import kotlinx.html.dom.create
import kotlinx.html.js.div
import kotlinx.html.js.onClickFunction
import kotlinx.html.js.tr
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLElement
import org.w3c.dom.HTMLTableRowElement

internal object RendererUtils {
    private val scope = CoroutineScope(Dispatchers.Main)

    fun drawSpinner(text: String) = document.create.div("spinner") {
        p("u-text-center") {
            +text
        }
        for (i in 1..5) { div("step$i") {} }
    }

    fun drawSpinnerSmall() = document.create.div("package-spinner") {
        for (i in 1..3) { div("bounce$i") {} }
    }

    fun buildTopicCard(name: String, description: String) = document.create.div("col-6") {
        div("tile tile--center") {
            div("tile__container") {
                p("tile__title u-no-margin") {
                    span("iconify") {
                        attributes["data-icon"] = "la:bookmark"
                        attributes["data-icon-inline"] = "true"
                    }
                    a("#", classes = "hide-visual") {
                        onClickFunction = { _ -> scope.launch { DOMManipulation.fetchTopicTable(name) } }
                        +name
                    }
                }
                p("tile__subtitle u-no-margin") {
                    +description
                    a("#", classes = "hide-visual") {
                        onClickFunction = { _ -> scope.launch { DOMManipulation.fetchTopicTable(name) } }
                        span("iconify") {
                            attributes["data-icon"] = "la:link-solid"
                            attributes["data-icon-inline"] = "true"
                        }
                    }
                }

                div {
                    id = "id-$name-pkglist"
                }
            }
        }

        div("card-separate")
    }

    fun buildTopicCards(cards: List<HTMLDivElement>) = document.create.div("row fluid-container").apply {
        cards.forEach { appendChild(it) }
    }

    fun buildTopicPackageTag(tag: String) = document.create.a("#", classes = "hide-visual") {
        onClickFunction = { _ -> scope.launch { DOMManipulation.searchThis(tag) } }
        div("tag tag--info") {
            +tag // TODO: text in DIV
        }
    }

    fun buildTopicPackageTags(tags: List<HTMLElement>) =
            document.create.div("tag-separate").apply {
                append(document.create.div("tag-container").apply {
                    tags.forEach { appendChild(it) }
                })
            }

    fun packageSearchTableRow(path: String, entry: String, name: String, index: Int) = document.create.tr {
        td("left-align") {
            a("/serve/$name/$index", classes = "code-link") {
                code("tex-path") { +path }
            }
        }
        td("left-align") {
            span("iconify") {
                attributes["data-icon"] = "la:chevron-right-solid"
                attributes["data-icon-inline"] = "true"
            }
            +entry
        }
        td {
            a("/serve/$name/$index", classes = "hide-visual") {
                span("iconify") {
                    attributes["data-icon"] = "la:eye"
                    attributes["data-icon-inline"] = "true"
                }
            }
        }
    }

    fun packageSearchBuildTable(rows: List<HTMLTableRowElement>) = document.create.table("table") {
        thead {
            tr {
                th(classes = "left-align") {
                    +"Location"
                }
                th(classes = "left-align") {
                    +"Description"
                }
                th {
                    +"Link"
                }
            }
        }
        tbody { }
    }.apply {
        this.lastChild?.run {
            rows.forEach { appendChild(it) }
        }
    }

    fun drawPackageTableRow(name: String) = document.create.tr {
        td("left-align") {
            div("tag tag--info") {
                +name // TODO: should that be a p?
            }
        }
        td {
            a("#", classes = "hide-visual") {
                onClickFunction = { _ -> scope.launch { DOMManipulation.searchThis(name) } }
                span("iconify") {
                    attributes["data-icon"] = "la:eye"
                    attributes["data-icon-inline"] = "true"
                }
            }
        }
    }

    fun drawPackageTable(rows: List<HTMLTableRowElement>) = document.create.div("content") {
        table("table striped") {
            thead {
                tr {
                    th(classes = "left-align") {
                        +"Package"
                    }
                    th {
                        +"Link"
                    }
                }
            }
            tbody { }
        }
    }.apply {
        this.lastChild?.run {
            rows.forEach { appendChild(it) }
        }
    }
}
