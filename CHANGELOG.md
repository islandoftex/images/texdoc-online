# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.6.1] - 2025-02-12

### Changed

* Added hyperlink to entry location for easier navigation.
  (see #15)

## [0.6.0] - 2024-07-21

### Changed

* Files now provide their file name as known in the TeX Live tree instead of
  the numeric file name.
  (see #14)
* Dependencies have been updated. This project now uses Kotlin 1.9.
* CI now uses Eclipse Temurin 17 as JDK.

## [0.5.0] - 2021-11-11

### Added

* The `/tds` endpoint now allows to access arbitrary files from the TDS texmf.
  (see #6)
* An OpenSearch XML specification is now provided so that texdoc-online might
  be used as a search engine.
  (see #8)

### Changed

* Dependencies have been updated. This project now uses Kotlin 1.5.

### Fixed

* Formatting problems of the API documentation have been corrected.

## [0.4.0] - 2020-12-14

### Added

* Fonts may be hosted locally by providing `css/fonts.css` with glue CSS code
  for the fonts Nunito Sans and Montserrat. Local fonts are preferred over
  fetching remote fonts.
  (see #4)

### Changed

* Rename `js/client.js` to more unique `js/texdoc-online.js`.
* `/pkg/name` will now redirect permanently to `/serve/name/0` instead of
  temporarily.
* Lookup of scripts is first performed locally before fetching remote scripts.
  (see #4)
* By default, the Docker image serves a local copy of the fonts and scripts
  from the `/texdoconline/includes` directory. This directory is used as
  resource directory by default. Please note that this means you need local
  copies of the fonts and scripts in your resource directory as well if you
  mount your own includes directory and wish to serve local scripts and fonts.
  (see #4)

### Fixed

* The `missing.html` page has not been served with `integrity` information.
* `missing.html` could not be overridden by include directory.

## [0.3.1] - 2020-10-13

### Fixed

* JavaScript has been served without any `integrity` information. This exposed
  users to avoidable security threats and has been fixed.
  (see #3)

## [0.3.0] - 2020-10-04

### Added

* docker-compose file for easier setup with reverse proxying for HTTPS support.

### Changed

* The default user is now `texdoconline` and not `root` which restricts
  possible manipulations in the image. This

### Fixed

* TeX Live has not been updated due to a wrong cron job configuration. The
  daily updating behavior has been fixed.

## [0.2.0] - 2020-09-09

### Added

* Command-line options to specify the host (`h`) and the port (`p`) when
  starting up the server from the JAR file.
* Overwrite flag to serve content from a folder (`s`) before trying to serve
  resources which enables serving alternative pages, CSS and JavaScript. The
  path `/js/client.js` should not be overwritten as this serves the software's
  main component to be included in custom pages.

### Changed

* The default host is now `0.0.0.0` instead of the local loopback to allow
  easier configuration of Docker redirects.
* All scripts are now located at `/texdoconline/` instead of `/usr/local/bin/`.
  For possible includes using the new `-s` option `/texdoconline/includes/`
  has been prepared.

## [0.1.0] - 2020-08-30

### Added

* Web frontend to the texdoc utility.
* API providing access to TeX documentation.

[Unreleased]: https://gitlab.com/islandoftex/images/texdoc-online/compare/v0.6.1...master
[0.6.1]: https://gitlab.com/islandoftex/images/texdoc-online/compare/v0.6.0...v0.6.1
[0.6.0]: https://gitlab.com/islandoftex/images/texdoc-online/compare/v0.5.0...v0.6.0
[0.5.0]: https://gitlab.com/islandoftex/images/texdoc-online/compare/v0.4.0...v0.5.0
[0.4.0]: https://gitlab.com/islandoftex/images/texdoc-online/compare/v0.3.1...v0.4.0
[0.3.1]: https://gitlab.com/islandoftex/images/texdoc-online/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/islandoftex/images/texdoc-online/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/islandoftex/images/texdoc-online/compare/v0.1.0...v0.2.0
[0.1.0]: https://gitlab.com/islandoftex/images/texdoc-online/-/tags/v0.1.0
